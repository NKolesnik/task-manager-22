package ru.t1consulting.nkolesnik.tm.command.task;

import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-index";

    public static final String DESCRIPTION = "Remove task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().removeByIndex(userId, index);
    }

}
