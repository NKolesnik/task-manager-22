package ru.t1consulting.nkolesnik.tm.command.user;

import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-view-profile";

    public static final String DESCRIPTION = "Display user info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
