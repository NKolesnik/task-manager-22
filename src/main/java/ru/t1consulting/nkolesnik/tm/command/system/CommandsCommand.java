package ru.t1consulting.nkolesnik.tm.command.system;

import ru.t1consulting.nkolesnik.tm.api.model.ICommand;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show command list.";

    public static final String ARGUMENT = "-c";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (ICommand command : commands) {
            String name = command.getName();
            if (name == null || name.isEmpty()) {
                continue;
            }
            System.out.println(name);
        }
    }

}
