package ru.t1consulting.nkolesnik.tm.repository;

import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.stream.Collectors;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        return models.stream()
                .filter(user->login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models.stream()
                .filter(user->email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(final String login) {
        return models.stream()
                .anyMatch(item->login.equals(item.getLogin()));
    }

    @Override
    public boolean isEmailExist(final String email) {
        return models.stream()
                .anyMatch(item->email.equals(item.getEmail()));
    }

}
