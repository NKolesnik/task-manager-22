package ru.t1consulting.nkolesnik.tm.service;

import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(
            final String userId,
            final String name,
            final String description,
            final Date dateBegin,
            final Date dateEnd
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        final Project project = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        final Project project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx->idx>-1).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        final Project project = Optional.ofNullable(findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx->idx>-1).orElseThrow(IndexIncorrectException::new);
        final Project project = Optional.ofNullable(findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(ProjectIdEmptyException::new);
        final Project project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

}
