package ru.t1consulting.nkolesnik.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

}
