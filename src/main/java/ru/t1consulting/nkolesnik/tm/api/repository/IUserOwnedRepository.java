package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findByIndex(String userId, Integer index);

    M findById(String userId, String id);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    void clear(String userId);

    long getSize(String userId);

}
