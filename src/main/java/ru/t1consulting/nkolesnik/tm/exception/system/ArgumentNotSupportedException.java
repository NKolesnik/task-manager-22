package ru.t1consulting.nkolesnik.tm.exception.system;

import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
